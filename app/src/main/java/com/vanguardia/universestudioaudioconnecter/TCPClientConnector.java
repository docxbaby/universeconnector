package com.vanguardia.universestudioaudioconnecter;

import android.widget.Button;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;



public class TCPClientConnector {

    private AppCompatActivity context;
    private TextView connInfo;

    public TCPClientConnector(AppCompatActivity context){
    this.context = context;

    }

    public boolean connect(TextView connInfo){
        ExecutorService executor = null;
        // Get the result once the task completes
        try {
            // Execute the task asynchronously
            executor = Executors.newSingleThreadExecutor();
            Future<String> futureResult = executor.submit(new NetworkTask());
            String result = futureResult.get();
            connInfo.setText(result);
            // Perform post-processing with the result
            Toast.makeText(this.context, result, Toast.LENGTH_SHORT).show();
            return true;
        } catch (Exception e) {
            // Handle any exceptions that occurred during the task execution
            Toast.makeText(this.context, "Hubo un error al conectar al servidor TCP", Toast.LENGTH_SHORT).show();
            Toast.makeText(this.context, e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            if (executor != null){
                executor.shutdown();
            }
        }
        return false;
    }

    /**
     * Ejecusión de tareas en segundo plano
     * */
    private class NetworkTask implements Callable<String> {
        @Override

        public String call() throws Exception {
            TCPClient tcpClient = new TCPClient();
            tcpClient.connect(connInfo);
            return "Task completed successfully";
        }

    }
}
