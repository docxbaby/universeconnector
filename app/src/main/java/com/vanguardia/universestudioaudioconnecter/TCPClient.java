package com.vanguardia.universestudioaudioconnecter;

import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class TCPClient {
    private static final String SERVER_IP = "192.168.100.5"; // IP address of the server
    private static final int SERVER_PORT = 9999; // Port number of the server

    public void connect(TextView connInfo) {
        try {
            // Connect to the server
            Socket socket = new Socket(SERVER_IP, SERVER_PORT);
            System.out.println("Connected to server.");

            // Get the output stream for writing to the server
            OutputStream outputStream = socket.getOutputStream();

            // Prepare the message to send
            String message = "Hello, server!";
            byte[] data = message.getBytes();

            // Send the message to the server
            outputStream.write(data);
            System.out.println("Sent: " + message);

            // Get the input stream for reading from the server
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            // Read the response from the server
            String response = reader.readLine();
            connInfo.setText(connInfo.getText() + response );
            System.out.println("Received: " + response);

            // Close the connection
            socket.close();
        } catch (UnknownHostException ue){
            ue.printStackTrace();
        }

        catch (IOException e) {
            e.printStackTrace();

        }
    }

}