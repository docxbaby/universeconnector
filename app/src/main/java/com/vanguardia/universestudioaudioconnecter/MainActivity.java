package com.vanguardia.universestudioaudioconnecter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Switch;
import android.widget.CompoundButton;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
    AppCompatActivity activity = this;
    TextView infoBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Switch switchServer = findViewById(R.id.switchServer);
        infoBox = findViewById(R.id.infoBox);

        //incio del server
        switchServer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    System.out.println("Switch on");
                    TCPClientConnector tcpConnector = new TCPClientConnector(activity);
                    tcpConnector.connect(infoBox);
                }
                 else {
                }
            }
        });// fin del switch
    }
}